-- LUALOCALS < ---------------------------------------------------------
local error, math, minetest, pairs, rawget, string, table, tostring,
      type
    = error, math, minetest, pairs, rawget, string, table, tostring,
      type
local math_floor, string_format, string_gsub, string_match, string_sub,
      table_concat, table_sort
    = math.floor, string.format, string.gsub, string.match, string.sub,
      table.concat, table.sort
-- LUALOCALS > ---------------------------------------------------------

local function sortedpairs(tbl)
	local keys = {}
	for k in pairs(tbl) do keys[#keys + 1] = k end
	table_sort(keys)
	local i = 0
	return function()
		i = i + 1
		local k = keys[i]
		if k == nil then return end
		return k, tbl[k]
	end
end

local bytype = {}

local function dumptablecore(obj, ctx)
	local bt = bytype[type(obj)]
	if bt then return bt(obj, ctx) end
	return error(string_format("unsupported type %q", type(obj)))
end

function bytype.string(obj, ctx)
	obj = minetest.get_translated_string("en", obj)
	if #obj <= 60 then return string_gsub(string_format("%q", obj), "\\\n", "\\n") end
	local parts = {}
	for i = 1, #obj, 60 do
		parts[#parts + 1] = string_gsub(string_format("%q",
				string_sub(obj, i, i + 59)), "\\\n", "\\n")
	end
	return table_concat(parts, "..\n" .. ctx.indent)
end

bytype.boolean = tostring
bytype.number = tostring

local function dumptableraw(obj, ctx)
	local ents = {}
	local oldindent = ctx.indent
	ctx.indent = ctx.indent .. "\t"
	for i = 1, #obj do
		local oldpath = ctx.path
		ctx.path = oldpath and (oldpath .. "_" .. i) or i
		ents[#ents + 1] = dumptablecore(rawget(obj, i), ctx)
		ctx.path = oldpath
	end
	for k in sortedpairs(obj) do
		if type(k) ~= "number" or k < 1 or k > #obj or k ~= math_floor(k) then
			local oldpath = ctx.path
			local nk = string_gsub(k, "%W", "_")
			ctx.path = oldpath and (oldpath .. "_" .. nk) or nk
			if type(k) == "string" and string_match(k, "^[A-Za-z_][A-Za-z0-9_]*$") then
				ents[#ents + 1] = k .. " = " .. dumptablecore(rawget(obj, k), ctx)
			else
				ents[#ents + 1] = "[" .. dumptablecore(k, ctx)
				.. "] = " .. dumptablecore(rawget(obj, k), ctx)
			end
			ctx.path = oldpath
		end
	end
	ctx.indent = oldindent
	local short = "{" .. table_concat(ents, ", ") .. "}"
	if #short <= 60 then return short end
	return "{" .. table_concat(ents, ",\n" .. ctx.indent) .. "}"
end

function bytype.table(obj, ctx)
	if ctx.locals then
		local oldindent = ctx.indent
		ctx.indent = "\t"
		local oldlocals = ctx.locals
		ctx.locals = nil
		local raw = dumptableraw(obj, ctx)
		ctx.locals = oldlocals
		ctx.indent = oldindent
		local found = ctx.locals(raw, ctx, obj)
		if found ~= nil then return found end
	end
	return dumptableraw(obj, ctx)
end

bytype["nil"] = function() return "nil" end
bytype["function"] = bytype["nil"]
bytype["userdata"] = bytype["nil"]

local function dumptable(obj, ctx, ...)
	ctx = ctx or {}
	ctx.indent = ctx.indent or "\t"
	return dumptablecore(obj, ctx, ...)
end

return dumptable, sortedpairs
