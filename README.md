# Node/Item Definition Ripper

*You're making a puzzle/adventure game, and you need building materials for the scenery.  It's a bother to make all original artwork, or you like the look and feel of something that already exists, but it comes with all this extra functionality that you don't want...*

This mod will allow you to export the "superficial" definitions and media for selected items.  This includes visuals, sounds, and basic physical properties (solid/liquid/gas, climbable, lighting) but none of the callbacks or other gameplay behavior.  This makes them ideal for:

- "inert" decorative nodes in "adventure" gameplay modes where most of the scenery is non-interactable and only a few puzzle pieces have specific mechanics.
- exporting your sculptures and builds as playable games (e.g. using modgen to convert a build into a mapgen mod) with only carefully chosen mechanics receated (e.g. working doors for access) but without the rest.
- pruning a complex game, with a lot of unused items, definitions, and mechanics, for smaller downloads, faster load times, and reduced memory usage.

The hope is that this mod will allow experienced creative-mode builders to use familiar tools to build scenery, backgrounds, maps and levels, and then export those things for use in games.  Formats like schematics, worldedit or modgen provide the "geometry" aspect of map export; this mod can be used to get the "material" aspect.

## Usage

Add the mod to a game that has all the things loaded that you want to export, instruct the mod which items to export (using chat commands), and it will dump the start of a mod that can be used to import item registrations into your game.

You will get a folder in your worldpath containing a skeletal mod:

- All media files referenced by the items, split into textures/sounds/models.
- An `exported.lua` file with each definition.
- A `mediasource.json` file that identifies the mod source of each media file included, for help tracing licensing and attribution.
- A `defripper.json` file that stores the configuration, and can be used to "rehydrate" the configuation in another world with the same mods installed, making it possible to incrementally extend the extracted data without needing to keep the entire original world around.

You will need to provide your own `init.lua`, `mod.conf` and other infrastructure, but the exported definitions are kept in a separate file so you can safely overwrite it later (e.g. if you add definitions) without destroying your custom logic.

`exported.lua` takes a register_item-like function as a file-level parameter.

- This function will receive a single parameter with the definition table; it does NOT get an item name.
- There is a `_raw_name` key inside the definition from which you will need to derive your own name in a manner you deem appropriate (you have an opportunity to customize before registration here).
- Aliases are exported using the same format, but they will only have an `_alias_to` that will point to the _raw_name of the node the alias is pointing to.

Example:
```lua
local modname = minetest.get_current_modname()
loadfile(minetest.get_modpath(modname) .. "/exported.lua")(function(def)
	local myname = modname .. ":" .. def._raw_name:gsub(":", "__")
	if def._alias_to then
		local myalias = modname .. ":" .. def._alias_to:gsub(":", "__")
		return minetest.register_alias(myname, myalias)
	end
	def._raw_name = nil
	return minetest.register_item(myname, def)
end)
```

By keeping your custom logic and overrides in a separate set of files, and not modifying the defripper output files, you can just delete and reimport the auto-generated content any time you want to change the set of items you're exporting, or any time you pull updates from upstream and want to marge them into your own version.

## Licensing Warning

You are still responsible for complying with licensing for all media files in the export.  This includes:

- Tracing the origin of each file (the names should match, and be unique in any sane setup) and identifying the author and original license.
- Including attribution/credit in your resultant project.
- Complying with sublicensing requirements when selecting the license for your own projet containing these media.
- Anything else required by the license that applies to any file you use.

This tool may also rip media from "private" mods you might have installed, including things you don't actually have the right to redistribute; you are responsible for manually ensuring these are not included in any product you create using this tool.

## Commands

Minetest "items" includes all nodes, craftitems and tools.  The definition ripper mod supports exporting all of them, though it is more focused on nodes.

### Basic Commands

Hand-pick which definitions to export.

- `/defripper` - re-export all item definitions already marked for export (saved in mod storage).
- `/defripper_clear` - clear all saved definitions.
- `/defripper_add <pattern>` - add all definitions whose technical name (modname:item_name) matches the given lua pattern.
- `/defripper_rm <pattern>` - remove all definitions whose technical name (modname:item_name) matches the given lua pattern.
- `/defripper_inv` - add all definitions for items currently in the player's inventory.

### Area-Scanning Commands

Automatically export all materials used in an area.

For each of these commands, if the `defripper_node_inv` setting is `true` (default `false`), it will descend into node meta inventories and rip items found there as well.

- `/defripper_here [rx [ry [rz]]]` - rip all nodes/items within a cuboid/rectanguloid centered around the player, right now.
- `/defripper_step [rx [ry [rz [step]]]]` - rip all nodes/items within a cuboid/rectanguloid centered around the player, continuously, every time the player moves `step` nodes, until the server is restarted or command is re-run to change it.  `/defripper_step 0` disables it.

### Extra Media Commands

Add additional media to the export, even if not referenced directly by a definition.

This is useful if you intend to recreate some mechanics/behavior in the downstream product, and want to include the media associated with it that aren't directly referenced by the definition itself.  Example: door opening/closing sounds.

- `/defripper_m_add <pattern>` - add all media whose filename matches the given lua pattern.
- `/defripper_m_rm <pattern>` - remove all media whose filename matches the given lua pattern.

## Advanced Usage

### Recreating World for Updates

It is not necessary to keep the original world you used defripper on to update your definitions, so long as you can recreate that world with the same mods.

- The defripper export contains its config, and you can copy it all back into the corresponding dir in the recreated world to restore state.
- defripper will warn you if there are any mods you're missing when trying to re-export.  Finding the correct version, when forks exist, is still your responsibility, so you may want to keep note of which versions things were based on.
- Media files are NOT updated by default if they already exist in the dump, so you can use alternative or texturepack versions if you want.  New ones are still added, and unused ones are still pruned.  If you want to force an update, remove the affected media from the export.

### Custom Property Filtering

A `customfilter.lua` file can be loaded in the `defripper` dir inside the world path.  If present, it will be loaded each time definitions are dumped.  It must return a function, which:

- Takes `(filtered, name, def)` as input parameters
  - `filtered` is the definition to dump, pre-filtered by defripper.
  - `name` is the original name of the registered item.
  - `def` is the original registered item definition.
- Returns one of:
  - `nil` to keep the existing `filtered` table for the export (which can also be modified by the filter function in-place).
  - `false` to exclude this item entirely from export.
  - any **table**, to replace the filtered table in the export.

Example `customfilter.lua` that excludes all non-node items:

```lua
return function(filtered, name, def)
	if def.type ~= "node" then return false end
	return filtered -- or nil
end
```