-- LUALOCALS < ---------------------------------------------------------
local error, io, minetest, pairs, rawset, setmetatable, string
    = error, io, minetest, pairs, rawset, setmetatable, string
local io_open, string_format
    = io.open, string.format
-- LUALOCALS > ---------------------------------------------------------

local include = ...
local getdir = include("fileops")

local modname = minetest.get_current_modname()
local mydir = getdir(minetest.get_worldpath() .. "/" .. modname)
local dbpath = mydir .. "/" .. modname .. ".json"

------------------------------------------------------------------------

local function read_config()
	local fh = io_open(dbpath, "r")
	if not fh then return end
	local s = fh:read("*all")
	fh:close()
	return minetest.parse_json(s)
end

local function read_legacy_json()
	local fh = io_open(mydir .. "/export.json", "r")
	if not fh then return end
	local s = fh:read("*all")
	fh:close()
	s = minetest.parse_json(s)
	return s and {items = s}
end

local function read_legacy_storage()
	local modstore = minetest.get_mod_storage()
	local s = modstore:get_string("export")
	s = s and s ~= "" and minetest.deserialize(s) or {}
	return s and {items = s}
end

------------------------------------------------------------------------

local configdb = read_config() or read_legacy_json() or read_legacy_storage()

local function savedb()
	minetest.safe_file_write(dbpath, minetest.write_json(configdb, true))
end

------------------------------------------------------------------------

local allkeys = {
	items = {},
	media = {},
	props = false,
}
setmetatable(configdb, {
		__newindex = function(t, k, v)
			if not allkeys[k] then
				error(string_format("disallowed configdb key %q", k))
			end
			return rawset(t, k, v)
		end
	})
for k, v in pairs(allkeys) do
	if v and not configdb[k] then
		configdb[k] = v
	end
end

------------------------------------------------------------------------

return configdb, savedb
