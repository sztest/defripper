-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, rawget, rawset
    = minetest, pairs, rawget, rawset
-- LUALOCALS > ---------------------------------------------------------

local include = ...

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

local configdb, savedb = include("configdb")
local exportall = include("exportall")

function api.item_get_all()
	local t = {}
	for k in pairs(configdb.items) do t[k] = true end
	return t
end

function api.item_get(k) return configdb.items[k] end

function api.item_set(k, v)
	configdb.items[k] = v and true or nil
end

function api.export()
	savedb()
	return exportall()
end
