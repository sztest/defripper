-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest
    = ipairs, minetest
-- LUALOCALS > ---------------------------------------------------------

local mediadirs = {
	sounds = true,
	textures = true,
	models = true,
	locale = true,
	media = true,
}

local mediacache = {}
do
	local function scan(path, modname)
		for _, v in ipairs(minetest.get_dir_list(path, false)) do
			mediacache[v] = {path = path .. "/" .. v, modname = modname}
		end
		for _, v in ipairs(minetest.get_dir_list(path, true)) do
			scan(path .. "/" .. v, modname)
		end
	end
	for _, n in ipairs(minetest.get_modnames()) do
		scan(minetest.get_modpath(n), n)
	end
end

return mediacache, mediadirs
