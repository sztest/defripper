-- LUALOCALS < ---------------------------------------------------------
local error, loadfile, minetest, unpack
    = error, loadfile, minetest, unpack
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local include
do
	local modpath = minetest.get_modpath(modname)
	local included = {}
	include = function(n)
		local found = included[n]
		if found ~= nil then return unpack(found) end
		local func, err = loadfile(modpath .. "/" .. n .. ".lua")
		if not func then error(err) end
		found = {func(include)}
		included[n] = found
		return unpack(found)
	end
end

include("commands")
include("api")
